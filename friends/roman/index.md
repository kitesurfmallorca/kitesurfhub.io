---
layout: gallery
title: "Kite-Mallorca - Friends - Roman"
subtitle: 
date: 
modified:
description: "Kitesurfing is a great sport and in combination with water, waves and wind very well suited for brilliant photos. When do you make your pictureswith us?"
dataFile: roman
image:
  background: 3.jpg
  feature: roman.jpg
  title: Romans Trommelwerk... Yeah!!!
  teaser: 
  thumb: 
snippets: true
lang: de
en-url: "friends/roman"
es-url: "friends/roman"
de-url: "friends/roman"
t:
  link1: "kitekurse"
  menutxt1: "kitekurse"
  link2: "vermietung"
  menutxt2: "vermietung"
  link3: "wind"
  menutxt3: "wind"
  link4: "kontakt"
  menutxt4: "kontakt"
  link5: "flying-friends"
  menutxt5: "bilder & videos"
  link6: "datenschutz"
  menutxt6: "Datenschutz"
  link7: "kontaktiere-uns"
  menutxt7: "Kontaktiere uns"
  teaser: Erinnerungen an unserem lieben verrückten Professor... XXIXIX
---

### Roman
